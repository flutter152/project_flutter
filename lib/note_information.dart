import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:example_project/note_form.dart';
import 'package:flutter/material.dart';

class NoteInformation extends StatefulWidget {
  NoteInformation({Key? key}) : super(key: key);

  @override
  _NoteInformationState createState() => _NoteInformationState();
}


class _NoteInformationState extends State<NoteInformation> {

  Color yellow = Colors.yellow.shade100;

  final Stream<QuerySnapshot> _noteStream = FirebaseFirestore.instance
      .collection('notes')
      .orderBy('title', descending: true)
      .snapshots();
  CollectionReference notes = FirebaseFirestore.instance.collection('notes');
  Future<void> delNote(noteId) {
    return notes
        .doc(noteId)
        .delete()
        .then((value) => print('Note Delete'))
        .catchError((error) => print('Failed to delete note: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _noteStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading..');
          }
          return Container(
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                        children: snapshot.data!.docs.map((DocumentSnapshot document) {
                          Map<String, dynamic> data =
                              document.data()! as Map<String, dynamic>;
                            return ListTile(
                              title: Text(data['title']),
                              subtitle: Text(data['date'].toDate().toString(),style: TextStyle(fontSize: 15)),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => NoteForm(
                                              noteId: document.id,
                                )));
                              },
                              trailing: IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () => showDialog<String>(
                                  context: context,
                                  builder: (BuildContext context) => AlertDialog(
                                    backgroundColor: Color(0xFFfaeedc),
                                    title: const Text('Delete Note',style: TextStyle(fontWeight: FontWeight.bold),),
                                    content: const Text('are u sure delete this note :c'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () => Navigator.pop(context, 'Cancel'),
                                        child: const Text('Cancel',style: TextStyle(color: Colors.amber),),
                                        
                                      ),
                                      TextButton(
                                        onPressed: () => Navigator.pop(context, 'OK'),
                                        child: const Text('OK',style: TextStyle(color: Colors.red),),
                                      ),
                                    ],
                                  ),
                                ),
                                  // delNote(document.id);
                              ),
                            );
                  }).toList(),
                  )
                )
              ], 
            ),
          );
          
          // return ListView(
          //   children: snapshot.data!.docs.map((DocumentSnapshot document) {
          //     Map<String, dynamic> data =
          //         document.data()! as Map<String, dynamic>;
          //       return ListTile(
                  
          //         title: Text(data['title']),
          //         subtitle: Text(data['date'].toDate().toString(),style: TextStyle(fontSize: 15)),
          //         onTap: () {
          //           Navigator.push(
          //               context,
          //               MaterialPageRoute(
          //                   builder: (context) => NoteForm(
          //                         noteId: document.id,
          //           )));
          //         },
          //         trailing: IconButton(
          //           icon: Icon(Icons.delete),
          //           onPressed: () => showDialog<String>(
          //             context: context,
          //             builder: (BuildContext context) => AlertDialog(
          //               backgroundColor: Color(0xFFfaeedc),
          //               title: const Text('Delete Note',style: TextStyle(fontWeight: FontWeight.bold),),
          //               content: const Text('are u sure delete this note :c'),
          //               actions: <Widget>[
          //                 TextButton(
          //                   onPressed: () => Navigator.pop(context, 'Cancel'),
          //                   child: const Text('Cancel',style: TextStyle(color: Colors.amber),),
                            
          //                 ),
          //                 TextButton(
          //                   onPressed: () => Navigator.pop(context, 'OK'),
          //                   child: const Text('OK',style: TextStyle(color: Colors.red),),
          //                 ),
          //               ],
          //             ),
          //           ),
          //             // delNote(document.id);
          //         ),
          //       );
              
          //   }).toList(),
          // );
        });
  }
}
