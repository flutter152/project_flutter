import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class NoteForm extends StatefulWidget {
  String noteId;
  NoteForm({Key? key, required this.noteId}) : super(key: key);

  @override
  _NoteFormState createState() => _NoteFormState(this.noteId);
}

class _NoteFormState extends State<NoteForm> {
  String textTime = "Select YY-MM-DD || Time.";
  DateTime selectDate = DateTime.now();
  String noteId;
  String title = "";
  String content = "";
  CollectionReference notes = FirebaseFirestore.instance.collection('notes');
  _NoteFormState(this.noteId);
  TextEditingController _titleController = new TextEditingController();
  TextEditingController _contentController = new TextEditingController();
  //TextEditingController _ageController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    if (this.noteId.isNotEmpty) {
      notes.doc(this.noteId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          title = data['title'];
          content = data['content'];
          selectDate = data['date'].toDate();
          textTime = this.selectDate.toString();
          _titleController.text = title;
          _contentController.text = content;
        }
      });
    }
  }

  final _formkey = GlobalKey<FormState>();

  Future<void> addNote() {
    return notes
        .add({
          'date': this.selectDate,
          'title': this.title,
          'content': this.content,
        })
        .then((value) => print('Note Added'))
        .catchError((error) => print('Failed to add note: $error'));
  }

  Future<void> updateNote() {
    return notes
        .doc(noteId)
        .update({
          'date': this.selectDate,
          'title': this.title,
          'content': this.content,
        })
        .then((value) => print('Note Updateed'))
        .catchError((error) => print('Failed to add note: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFfaeedc),
        appBar: AppBar(title: Text('Note'),backgroundColor: Color(0xFF89580b)),
        floatingActionButton: FloatingActionButton(
                        backgroundColor: Colors.red,
                        onPressed: () async {
                            if (_formkey.currentState!.validate()) {
                              if (noteId.isEmpty) {
                                await addNote();
                              } else {
                                updateNote();
                              }
                              Navigator.pop(context);
                            }
                          },
                        tooltip: 'Save',
                        child: Icon(Icons.save),
        ),
        body: Container(
          child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(16.0),
              child: Form(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  key: _formkey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: _titleController,
                        decoration: InputDecoration(labelText: 'Title'),
                        onChanged: (value) {
                          setState(() {
                            title = value;
                          });
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please input Title';
                          }
                          return null;
                        },
                      ),
                      Column(
                        children: [Text("")],
                      ),
                      TextButton(
                        onPressed: () {
                          DatePicker.showDateTimePicker(
                            context,
                            showTitleActions: true,
                            onChanged: (date) {
                              print('change $date in time zone ' +
                                  date.timeZoneOffset.inHours.toString());
                            },
                            onConfirm: (date) {
                              setState(() {
                                selectDate = date;
                                textTime = selectDate.toString();
                                print('confirm $textTime');
                              });
                            },
                            currentTime: selectDate,
                            locale: LocaleType.en,
                          );
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Icon(
                              Icons.calendar_today_outlined,
                              color: Color(0xFF5DB075),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 8),
                              child: Text(
                                textTime,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w400),
                              ),
                            )
                          ],
                        ),
                      ),
                      TextFormField(
                        controller: _contentController,
                        keyboardType: TextInputType.multiline,
                        maxLines: 18,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(labelText: 'Content',alignLabelWithHint: true),
                        onChanged: (value) {
                          setState(() {
                            content = value;
                          });
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please input Content';
                          }
                          return null;
                        },
                      ),
                       //
                      // ElevatedButton(
                      //     onPressed: () async {
                      //       if (_formkey.currentState!.validate()) {
                      //         if (noteId.isEmpty) {
                      //           await addNote();
                      //         } else {
                      //           updateNote();
                      //         }

                      //         Navigator.pop(context);
                      //       }
                      //     },
                      //     child: Text('Save'))
                    ],
                  )),
            )
          ],
        )));
  }
}
